
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Semaphore;


public class Server {
	public static int TAMANHO_MAXIMO_DO_SEGMENTO = 2;
	public static int QUANTIDADE_MAXIMA_DE_PACOTES = 5;
	public static int TAMANHO_DA_JANELA = 5;
	public static int TEMPO_DE_TIMEOUT = 50;
	public static int LIMITE_DE_PACOTES_POR_SEGUNDO = 5;
	public static int PACOTES_LIVRES_PARA_SER_ENVIADOS = LIMITE_DE_PACOTES_POR_SEGUNDO;
	public static Semaphore semaforoMutex = new Semaphore(1);
	public static int ULTIMO_ENVIADO = 0;
	
	public static void main(String[] args) throws Exception{
		int tamanhoDoPacote = 0;
		int pacoteEsperandoAck = 0;
		String dado; 
		ArrayList<Ack> bufferAck = new ArrayList<Ack>();
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite o dado a ser enviado: ");
		dado = scanner.nextLine();
		System.out.println("Diga a quantidade m�xima de pacotes a serem enviados: ");
		QUANTIDADE_MAXIMA_DE_PACOTES = scanner.nextInt();
		System.out.println("Diga a quantidade m�xima de pacotes a serem enviados por segundo para o cliente: ");
		PACOTES_LIVRES_PARA_SER_ENVIADOS = LIMITE_DE_PACOTES_POR_SEGUNDO = scanner.nextInt();
		System.out.println("Diga a quantidade de pacotes a serem enviados antes de receber a confirma��o de entrega: ");
		TAMANHO_DA_JANELA = scanner.nextInt();
		scanner.close();
		ArrayList<byte[]> bytesDoDado = new ArrayList<byte[]>();
		tamanhoDoPacote = dado.length()/QUANTIDADE_MAXIMA_DE_PACOTES;
		StringBuilder s = new StringBuilder("");
		s.append(dado);
		for(int i = 0; i < QUANTIDADE_MAXIMA_DE_PACOTES; i++) {
			int nextPos = i == QUANTIDADE_MAXIMA_DE_PACOTES - 1 ? s.length() : (i + 1)*(tamanhoDoPacote);
			String aux = s.substring(i*(tamanhoDoPacote), nextPos);
			bytesDoDado.add(aux.getBytes());
		}
		DatagramSocket socketUdp = new DatagramSocket();
		InetAddress enderecoDoCliente = InetAddress.getLocalHost();
		ArrayList<Pacote> enviados = new ArrayList<Pacote>();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(1000);
						semaforoMutex.acquire();
						if(ULTIMO_ENVIADO != bytesDoDado.size()) {
							PACOTES_LIVRES_PARA_SER_ENVIADOS = LIMITE_DE_PACOTES_POR_SEGUNDO;
						} else {
							break;
						}
						semaforoMutex.release();
					} catch (InterruptedException e) {
						//N�o deve fazer nada caso seja interrompido.
					}
				}
			}
		}).start();
		while (true) {
			if(PACOTES_LIVRES_PARA_SER_ENVIADOS > 0 ) {
				while (ULTIMO_ENVIADO < QUANTIDADE_MAXIMA_DE_PACOTES && TAMANHO_DA_JANELA > ULTIMO_ENVIADO - pacoteEsperandoAck) {
					byte[] filePacketBytes = new byte[bytesDoDado.get(ULTIMO_ENVIADO).length/QUANTIDADE_MAXIMA_DE_PACOTES];
					filePacketBytes = bytesDoDado.get(ULTIMO_ENVIADO);
					Pacote objetoDoPacote = new Pacote(filePacketBytes, ULTIMO_ENVIADO, (ULTIMO_ENVIADO == QUANTIDADE_MAXIMA_DE_PACOTES - 1 ? true : false));
					byte[] pacoteASerEnviado = Convert.toBytes(objetoDoPacote);
					DatagramPacket pacote = new DatagramPacket(pacoteASerEnviado, pacoteASerEnviado.length, enderecoDoCliente, 35000);
					System.out.println("Mandando o pacote " + ULTIMO_ENVIADO +  " com tamanho de " + pacoteASerEnviado.length + " bytes");
					enviados.add(objetoDoPacote);
					socketUdp.send(pacote);
					ULTIMO_ENVIADO++;
					semaforoMutex.acquire();
					PACOTES_LIVRES_PARA_SER_ENVIADOS--;
					semaforoMutex.release();
				}
			}
			byte[] bytesDoAck = new byte[100];
			DatagramPacket ack = new DatagramPacket(bytesDoAck, bytesDoAck.length);
			try {// Seta o timeout para mandar a janela de novo.
				socketUdp.setSoTimeout(TEMPO_DE_TIMEOUT);
				socketUdp.receive(ack);
				Ack objetoDoAck = (Ack) Convert.toObject(ack.getData());
				bufferAck.add(objetoDoAck);
				System.out.println("ACK do pacote " + objetoDoAck.getNumDoPacote() + " recebido.");
				if ((objetoDoAck.getNumDoPacote() == QUANTIDADE_MAXIMA_DE_PACOTES || objetoDoAck.isUltimo()) && ULTIMO_ENVIADO == QUANTIDADE_MAXIMA_DE_PACOTES){
					break;
				}
				pacoteEsperandoAck = Math.max(pacoteEsperandoAck, objetoDoAck.getNumDoPacote());
			} catch (SocketTimeoutException e){
				if(PACOTES_LIVRES_PARA_SER_ENVIADOS > 0) {
					for (int i = pacoteEsperandoAck; i < ULTIMO_ENVIADO; i++){
						byte[] pacoteASerEnviado = Convert.toBytes(enviados.get(i));
						DatagramPacket pacote = new DatagramPacket(pacoteASerEnviado, pacoteASerEnviado.length, enderecoDoCliente, 35000 );
						socketUdp.send(pacote);
						System.out.println("Reenviando o pacote de numero " + enviados.get(i).getNum() +  " com tamanho de " + pacoteASerEnviado.length + " bytes.");
						semaforoMutex.acquire();
						PACOTES_LIVRES_PARA_SER_ENVIADOS--;
						semaforoMutex.release();
					}
				}
			}
		}
		System.out.println("Transmiss�o finalizada.");
		
	}
	
}
