import java.io.Serializable;
import java.util.Arrays;


public class Pacote implements Serializable {

	public int num;
	
	public byte[] dado;
	
	public boolean ultimo;

	public Pacote(byte[] dado, int num, boolean ultimo) {
		super();
		this.num = num;
		this.dado = dado;
		this.ultimo = ultimo;
	}

	public int getNum() {
		return num;
	}

	public void setSeq(int seq) {
		this.num = seq;
	}

	public byte[] getData() {
		return dado;
	}

	public void setData(byte[] data) {
		this.dado = data;
	}

	public boolean isUltimo() {
		return ultimo;
	}

	public void setLast(boolean last) {
		this.ultimo = last;
	}

	@Override
	public String toString() {
		return "PacoteUDP: [num: " + this.num + ", data: " + Arrays.toString(this.dado)
				+ ", last: " + this.ultimo + "]";
	}
	
}
