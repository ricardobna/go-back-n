import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Convert{
	
	public static byte[] toBytes(Object objeto) throws IOException {
		ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
		ObjectOutputStream outputStream = new ObjectOutputStream(byteArray);
		outputStream.writeObject(objeto);
		return byteArray.toByteArray();
	}

	public static Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream byteArray = new ByteArrayInputStream(bytes);
		ObjectInputStream outputStream = new ObjectInputStream(byteArray);
		return outputStream.readObject();
	}

}
