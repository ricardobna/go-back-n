import java.io.Serializable;


public class Ack implements Serializable{
	
	private int numDoPacote;
	private boolean ultimo;

	public Ack(int numDoPacote, boolean ultimo) {
		super();
		this.numDoPacote = numDoPacote;
		this.ultimo = ultimo;
	}

	public int getNumDoPacote() {
		return numDoPacote;
	}

	public void setNumDoPacote(int pacote) {
		this.numDoPacote = pacote;
	}

	public boolean isUltimo() {
		return ultimo;
	}

	public void setUltimo(boolean ultimo) {
		this.ultimo = ultimo;
	}
	
	

}
