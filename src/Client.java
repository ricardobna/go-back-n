import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Scanner;


public class Client {
	public static double PROBABILIDADE_DE_DESCARTE = 0.1;
	public static int TAMANHO_DO_PACOTE = 100;

	public static void main(String[] args) throws Exception{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Diga a probabilidade de descarte de pacotes pelo cliente: ");
		PROBABILIDADE_DE_DESCARTE = scanner.nextDouble();
		scanner.close();
		DatagramSocket fromSender = new DatagramSocket(35000);
		byte[] receivedData = new byte[TAMANHO_DO_PACOTE];
		int pacoteEsperado = 0;
		ArrayList<Pacote> received = new ArrayList<Pacote>();
		boolean end = false;
		
		while(!end){
			System.out.println("Esperando pacote...");
			DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);
			fromSender.receive(receivedPacket);
			Pacote pacote = null;
			try {
				pacote = (Pacote) Convert.toObject(receivedPacket.getData());
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Pacote de numero " + pacote.getNum() + " recebido, " + (pacote.isUltimo() ? "e o ultimo" : "nao e o ultimo") + ", agora deve aguardar confirmacao.");
		
			if (pacote.getNum() == pacoteEsperado && pacote.isUltimo()) {
				pacoteEsperado++;
				received.add(pacote);
				System.out.println("Ultimo pacote recebido.");
				end = true;
			} else if (pacote.getNum() == pacoteEsperado){
				pacoteEsperado++;
				received.add(pacote);
				System.out.println("Pacote armazenado no buffer.");
			} else {
				System.out.println("Pacote nao estava em ordem, logo, foi descartado.");
			}
			Ack ackObject = new Ack(pacoteEsperado, pacote.isUltimo());
			byte[] ackBytes = Convert.toBytes(ackObject);
			DatagramPacket pacoteAck = new DatagramPacket(ackBytes, ackBytes.length, receivedPacket.getAddress(), receivedPacket.getPort());
			if (Math.random() > PROBABILIDADE_DE_DESCARTE) {
				fromSender.send(pacoteAck);
				System.out.println("Mandando Ack do pacote de numero " + pacoteEsperado+ " com  " + ackBytes.length  + " bytes.");
			}else{
				System.out.println("Ack do pacote de numero " + ackObject.getNumDoPacote() + " foi perdido.");
			}
		}
		
		String result = null;
		StringBuilder stringBuilder = new StringBuilder("");
		for(Pacote p : received){
			for(byte b: p.getData()){
				stringBuilder.append((char) b);
			}
		}
		result = stringBuilder.toString();
		System.out.println("Dado enviado: " + result);
	}
	
	
}
